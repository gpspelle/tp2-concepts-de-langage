#include <iostream>
#include <string>

class Nombre {
	public:
		Nombre(unsigned int n) {
			premier_ = new Chiffre( n );
		}

		Nombre(void) {
			premier_ = nullptr;
		}

		Nombre(const Nombre &n) {
			premier_ = nullptr;

			Chiffre *local = n.premier_;
			Chiffre *actual = n.premier_;
			while(actual != nullptr) {
				if (premier_ == nullptr) {
					premier_ = new Chiffre(actual->chiffre_);
					local = premier_;
				} else {
					local->suivant_ = new Chiffre(actual->chiffre_);
					local = local->suivant_;
				}
				
				actual = actual->suivant_;

			}
		}

		//~Nombre() { delete premier_; } // destructor
		~Nombre() { } // destructor

	    	friend std::ostream & operator <<(std::ostream & out, const Nombre & n) {
			if(n.premier_) {
				n.premier_->print(out);
			}
			return out;
		}

		friend std::istream &operator >>(std::istream & in, Nombre & n) {
			// in est le flux d'entrée
			
			Nombre tmp;
			Chiffre *actual = tmp.premier_;

			while(in.good()) {
				int c{ in.get() };
				if(std::isdigit( c )) {
					unsigned int d{ static_cast<unsigned int>( c - '0' )};
					if(actual == nullptr) {
						tmp.premier_ = new Chiffre(d);
						actual = tmp.premier_;
					} else {
						actual->suivant_ = new Chiffre(d);
						actual = actual->suivant_;
					}
				}
				
				else break;
			}

			actual = tmp.premier_;
			
			// Initialize current, previous and
			// next pointers
			Chiffre * current = actual;
			Chiffre *prev = nullptr, *next = nullptr;

			while (current != nullptr) {
				// Store next
				next = current->suivant_;

				// Reverse current node's pointer
				current->suivant_ = prev;

				// Move pointers one position ahead.
				prev = current;
				current = next;
			}

			actual = prev;
			n.premier_ = actual;

			return in;

		}

		friend void operator +=(Nombre & N, int n) {
			Chiffre * actual = N.premier_;
			Chiffre * before = N.premier_;

			int remaining = 0;
			while(actual != nullptr or n > 0) {
				unsigned int val;
				if(actual != nullptr) {
					val = actual->chiffre_ + (n % 10) + remaining;
					actual->chiffre_ = val % 10;
					remaining = val / 10;
					before = actual;
					actual = actual->suivant_;

				} else {
					val = 0 + (n % 10) + remaining;
					before->suivant_ = new Chiffre(val % 10);
					before = before->suivant_;
					remaining = val / 10;
				}

				n /= 10;
			}

			if(remaining) {
				before->suivant_ = new Chiffre(remaining);
			}
		}

		friend Nombre operator +(Nombre & N, int n) {

			int remaining = 0;

			Nombre *N_ = new Nombre{ N };
			Chiffre * actual = N_->premier_;
			Chiffre * before = N_->premier_;
			while(actual != nullptr or n > 0) {
				unsigned int val;
				if(actual != nullptr) {
					val = actual->chiffre_ + (n % 10) + remaining;
					actual->chiffre_ = val % 10;
					remaining = val / 10;
					before = actual;
					actual = actual->suivant_;

				} else {
					val = 0 + (n % 10) + remaining;
					before->suivant_ = new Chiffre(val % 10);
					before = before->suivant_;
					remaining = val / 10;
				}

				n /= 10;
			}

			if(remaining) {
				before->suivant_ = new Chiffre(remaining);
			}

			return *N_;
		}

		friend void operator +=(Nombre & N, Nombre & n) {
			Chiffre * actual0 = N.premier_;
			Chiffre * before0 = N.premier_;

			Chiffre * actual1 = n.premier_;
			Chiffre * before1 = n.premier_;

			int remaining = 0;
			int finished_first = 1;
			while(actual0 != nullptr or actual1 != nullptr) {
				unsigned int val;
				if(actual0 != nullptr and actual1 != nullptr) {
					val = actual0->chiffre_ + actual1->chiffre_ + remaining;
					actual0->chiffre_ = val % 10;
					remaining = val / 10;
					before0 = actual0;
					actual0 = actual0->suivant_;

					before1 = actual1;
					actual1 = actual1->suivant_;
				} else if(actual0 == nullptr) {
					val = (actual1->chiffre_ % 10) + remaining;
					before1->suivant_ = new Chiffre(val % 10);
					before0->suivant_ = before1->suivant_;
					before0 = before0->suivant_;
					before1 = before1->suivant_;
					remaining = val / 10;
					actual1 = actual1->suivant_;
					finished_first = 0;
				} else if(actual1 == nullptr) {
					val = (actual0->chiffre_ % 10) + remaining;
					before0->suivant_ = new Chiffre(val % 10);
					before1->suivant_ = before0->suivant_;
					before1 = before1->suivant_;
					before0 = before0->suivant_;
					remaining = val / 10;
					actual0 = actual0->suivant_;
					finished_first = 1;
				}
			}

			if(remaining) {
				if (finished_first == 1) {
					before0->suivant_ = new Chiffre(remaining);
				} else {
					before1->suivant_ = new Chiffre(remaining);
				}
			}
		}

		friend Nombre operator *(Nombre &N, unsigned int n) {

			Nombre *ans = new Nombre { 0 };
			int count = 0;
			while(n > 0) {
				int remaining = 0;
				Nombre tmp { N };

				Chiffre *actual = tmp.premier_;
				Chiffre *before = tmp.premier_;

				while(actual != nullptr) {
					unsigned int val = actual->chiffre_ * (n % 10) + remaining;
					actual->chiffre_ = val % 10;
					remaining = val / 10;
					before = actual;
					actual = actual->suivant_;
				}

				if(remaining) {
					before->suivant_ = new Chiffre(remaining);
				}


				for(int k = 0; k < count; k++) {
					actual = tmp.premier_;
					tmp.premier_ = new Chiffre(0);
					tmp.premier_->suivant_ = actual;
				}

				*ans += tmp;

				n /= 10;


				count++;
			}


			return *ans;
		}

		friend void operator *=(Nombre &N, unsigned int n) {

			Nombre copy { N };
			N = Nombre {0};
			
			int count = 0;
			while(n > 0) {
				unsigned int remaining = 0;
				Nombre tmp { copy };

				Chiffre *actual = tmp.premier_;
				Chiffre *before = tmp.premier_;

				while(actual != nullptr) {
					unsigned int val = actual->chiffre_ * (n % 10) + remaining;
					actual->chiffre_ = val % 10;
					remaining = val / 10;
					before = actual;
					actual = actual->suivant_;
				}


				if(remaining) {
					before->suivant_ = new Chiffre(remaining);
				}
				
				
				for(int k = 0; k < count; k++) {
					actual = tmp.premier_;
					tmp.premier_ = new Chiffre(0);
					tmp.premier_->suivant_ = actual;
				}
				N += tmp;
					
				n /= 10;
				count++;
			}
		}

	private:
	    struct Chiffre {
		unsigned int chiffre_;     // entre 0 et 9
		Chiffre * suivant_;        // chiffre de poids supérieur ou nullptr
		Chiffre(unsigned int n) {

			suivant_ = nullptr;
			unsigned int last_digit = n % 10;
			chiffre_ = last_digit;

			if (n/10 != 0) {
				suivant_ = new Chiffre(n/10);
			}
			
		}

		void print(std::ostream &out) {
			
			if (suivant_) {
				// if exists a next node
				suivant_->print(out);
			}

			out << chiffre_;
		}
	    };

	    Chiffre * premier_;

};

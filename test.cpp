#include <utility>
#include <sstream>
#include "gtest/gtest.h"
#include "Nombre.cpp"

Nombre factorielle(unsigned int n) {
	if(n == 1) {
		return Nombre(1);
	}

	Nombre n_1 = factorielle(n-1);
	return n_1 * n;
}

TEST(TestNombre, TestNombreVide) {
    Nombre n;
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "");
}

 
TEST(TestNombre, TestNombreNul) {
    Nombre n{ 0 };
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "0");
}


TEST(TestNombre, TestNombre12345678) {
    Nombre n{ 12345678 };
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreRef) {
    Nombre n1{ 12345678 };
    Nombre n2{ n1 };
    std::ostringstream os;
    os << n2;
    EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestLectureGrandNombre) {
    std::string big{ "123456789123456789123456789123456789" };
    std::istringstream in{ big };
    Nombre n;
    in >> n;
    std::ostringstream os;
    os << n;
    EXPECT_EQ(os.str(), big);
}

TEST(TestNombre, TestSum0) {
    int n = 13;
    Nombre N { 987 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1000");
}

TEST(TestNombre, TestSum1) {
    int n = 13412;
    Nombre N { 987 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "14399");
}

TEST(TestNombre, TestSum2) {
    Nombre n { 13 };
    Nombre N { 987 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1000");
}

TEST(TestNombre, TestSum3) {
    Nombre n { 987 };
    Nombre N { 13 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1000");
}

TEST(TestNombre, TestSum4) {
    int n = 13;
    Nombre N { 987 };

    N = N + n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1000");
}

TEST(TestNombre, TestSum5) {
    int n = 13412;
    Nombre N { 987 };

    N = N + n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "14399");
}

TEST(TestNombre, TestSum6) {
    Nombre n { 123421 };
    Nombre N { 987 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "124408");
}

TEST(TestNombre, TestSum7) {
    Nombre n { 0 };
    Nombre N { 987 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "987");
}

TEST(TestNombre, TestSum8) {
    Nombre n { 9999 };
    Nombre N { 1111 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "11110");
}

TEST(TestNombre, TestSum9) {
    Nombre n { 5 };
    Nombre N { 25 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "30");
}

TEST(TestNombre, TestSum10) {
    Nombre n { 25 };
    Nombre N { 5 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "30");
}

TEST(TestNombre, TestSum11) {
    Nombre n { 870 };
    Nombre N { 430 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1300");
}

TEST(TestNombre, TestSum12) {
    Nombre n { 999 };
    Nombre N { 1111 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "2110");
}

TEST(TestNombre, TestSum13) {
    Nombre n { 9999 };
    Nombre N { 111 };

    N += n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "10110");
}

TEST(TestNombre, TestMult0) {
    unsigned int n = 5;
    Nombre N { 5 };

    N = N * n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "25");
}

TEST(TestNombre, TestMult1) {
    unsigned int n = 50;
    Nombre N { 2 };

    N = N * n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "100");
}

TEST(TestNombre, TestMult2) {
    unsigned int n = 5;
    Nombre N { 200 };

    N = N * n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1000");
}

TEST(TestNombre, TestMult3) {
    unsigned int n = 25;
    Nombre N { 25 };

    N = N * n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "625");
}

TEST(TestNombre, TestMult4) {
    unsigned int n = 256;
    Nombre N { 256 };

    N = N * n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "65536");
}

TEST(TestNombre, TestMult5) {
    unsigned int n = 5;
    Nombre N { 5 };

    N *= n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "25");
}

TEST(TestNombre, TestMult6) {
    unsigned int n = 50;
    Nombre N { 2 };

    N *= n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "100");
}

TEST(TestNombre, TestMult7) {
    unsigned int n = 5;
    Nombre N { 200 };

    N *= n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1000");
}

TEST(TestNombre, TestMult8) {
    unsigned int n = 25;
    Nombre N { 25 };

    N *= n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "625");
}

TEST(TestNombre, TestMult9) {
    unsigned int n = 256;
    Nombre N { 256 };

    N *= n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "65536");
}

TEST(TestNombre, TestMult10) {

    unsigned int n = 15;
    std::string big{ "87178291200" };
    std::istringstream in{ big };
    Nombre N;
    in >> N;
    N *= n;
    std::ostringstream os;
    os << N;
    EXPECT_EQ(os.str(), "1307674368000");
}

TEST(TestNombre, TestFactorielle1){
    std::ostringstream os;
    os << factorielle(1);
    EXPECT_EQ(os.str(), "1");
}

TEST(TestNombre, TestFactorielle3){
    std::ostringstream os;
    os << factorielle(3);
    EXPECT_EQ(os.str(), "6");
}

TEST(TestNombre, TestFactorielle6){
    std::ostringstream os;
    os << factorielle(6);
    EXPECT_EQ(os.str(), "720");
}

TEST(TestNombre, TestFactorielle14){
    std::ostringstream os;
    os << factorielle(14);
    EXPECT_EQ(os.str(), "87178291200");
}

TEST(TestNombre, TestFactorielle15){
    std::ostringstream os;
    os << factorielle(15);
    EXPECT_EQ(os.str(), "1307674368000");
}

// 14! = 87178291200
// 15! = 1307674368000 = 15 * 14!

TEST(TestNombre, TestFactorielle123){
    std::ostringstream os;
    os << factorielle(123);
    EXPECT_EQ(os.str(), "12146304367025329675766243241881295855454217088483382315328918161829235892362167668831156960612640202170735835221294047782591091570411651472186029519906261646730733907419814952960000000000000000000000000000");
}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
